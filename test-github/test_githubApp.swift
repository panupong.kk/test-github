//
//  test_githubApp.swift
//  test-github
//
//  Created by Panupong Kukutapan on 16/6/2564 BE.
//

import SwiftUI

@main
struct test_githubApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
