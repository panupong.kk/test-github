//
//  ContentView.swift
//  test-github
//
//  Created by Panupong Kukutapan on 16/6/2564 BE.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
